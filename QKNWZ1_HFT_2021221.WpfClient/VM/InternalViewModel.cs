﻿using System.ComponentModel;
using System.Windows;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using QKNWZ1_HFT_2021221.Models;

namespace QKNWZ1_HFT_2021221.WpfClient.VM
{
	public partial class InternalViewModel : ObservableRecipient
	{
		private readonly (string, string) url;

		[ObservableProperty]
		private string input;

		public ImmutableRestCollection<ExpertGroup> Expertgroups { get; set; }
		public ImmutableRestCollection<Member> Members { get; set; }
		public ImmutableRestCollection<Country> Countries { get; set; }

		public static bool IsInDesignMode
		{
			get
			{
				var prop = DesignerProperties.IsInDesignModeProperty;
				return (bool)DependencyPropertyDescriptor.FromProperty(prop, typeof(FrameworkElement)).Metadata.DefaultValue;
			}
		}

		public InternalViewModel()
		{
			if (IsInDesignMode) return;
			this.url = ("http://localhost:49943/", "Internal/");
			this.Expertgroups = new(this.url.Item1, this.url.Item2);
			this.Members = new(this.url.Item1, this.url.Item2);
			this.Countries = new(this.url.Item1, this.url.Item2);
		}

		[ICommand]
		private void GetOneExpertgroup() => this.Expertgroups.GetSingle("GetOneExpertgroup/" + this.input);

		[ICommand]
		private void GetAllExpertgroups() => this.Expertgroups.ReInit();

		[ICommand]
		private void GetOneCountry() => this.Countries.GetSingle("GetOneCountry/" + this.input);

		[ICommand]
		private void GetAllCountries() => this.Countries.ReInit();

		[ICommand]
		private void GetOneMember() => this.Members.GetSingle("GetOneMember/" + this.input);

		[ICommand]
		private void GetAllMembers() => this.Members.ReInit();

		[ICommand]
		private void MembersInCapital() =>
			new NonCrudWindow(new ImmutableRestCollection<MemberCountry>(this.url.Item1, this.url.Item2, specialAction: "ListMembersInCapitalCity/true"))
			.ShowDialog();

		[ICommand]
		private void RichestMemberInExpertgroup() =>
			new NonCrudWindow(new ImmutableRestCollection<ExpertgroupMemberCountry>(this.url.Item1, this.url.Item2, specialAction: "GetRichestMemberInExpertGroup"))
			.ShowDialog();
	}
}
